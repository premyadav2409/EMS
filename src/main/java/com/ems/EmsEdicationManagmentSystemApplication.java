package com.ems;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EmsEdicationManagmentSystemApplication {

	public static void main(String[] args) {
		SpringApplication.run(EmsEdicationManagmentSystemApplication.class, args);
	}

}
