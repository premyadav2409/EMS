package com.ems.entitys;

import javax.persistence.Entity;
import javax.persistence.Id;

import lombok.Data;
import lombok.ToString;
@Entity
@Data
@ToString
public class Employee {
    @Id
	int id;
	String name;
	float sal;
}
