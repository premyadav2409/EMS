package com.ems.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.ems.entitys.Employee;
@Repository
public interface EmployeeRepo extends JpaRepository<Employee,Integer > {

}