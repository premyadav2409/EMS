package com.ems.web;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.ems.entitys.Employee;
import com.ems.repo.EmployeeRepo;
@Controller
public class HomeController {
	@Autowired
	public EmployeeRepo empRepo;
     @RequestMapping(value= {"/"})
	public String getHomeScreen(){
    	 System.out.println("raja");
		return "home";
	}
     @RequestMapping(value= {"/home"})
 	public @ResponseBody  String getHomeScreen1(){
    	 Optional<Employee> emp=empRepo.findById(1);
    	 System.out.println(emp.toString());
 		return "home";
 	}
}
